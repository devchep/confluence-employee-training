# Atlassian Plugin for managing employee training process
Use this plugin to manage employee training requests in your company
<div>
  <img src="https://images.prismic.io/smarttask%2F64db29c1-9bcb-4c61-83a6-fcf7e7dcb383_employee+engagement.gif" width="200" alt="Employee request gif"/>
</div>

## Roles
Here are general description of roles that involed in 'employee training' business process
### Employee
- view all available training programs
- make a request for a training program

**employee demo:** create application
<div>
  <img src="docs/createapplication.gif" width="600" alt="Create application gif"/>
</div>

### Team manager
- employee role features
- handle team training requests (reject or approve)

**team manager demo:** approve application
<div>
  <img src="docs/approval.gif" width="600" alt="Approve application gif"/>
</div>

### HR
- employee role features
- get all employee requests

**hr demo:** all applications endpoint
<div>
  <img src="docs/hr.png" width="600" alt="Hr endpoint img"/>
</div>

### Admin
- employee role features
- managing training programs (create, update, delete)

**admin demo:** create course demo
<div>
  <img src="docs/create.gif" width="600" alt="Create program gif"/>
</div>


**admin demo:** update course demo
<div>
  <img src="docs/edit.gif" width="600" alt="Update program gif"/>
</div>

**admin demo:** delete course demo
<div>
  <img src="docs/delete.gif" width="600" alt="Delete program gif"/>
</div>


## SDK commands 
SDK commands you'll use immediately:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-help  -- prints description for all commands in the SDK
