function setActiveNav() {
    var path = window.location;
    path = decodeURIComponent(path);
    $(".aui-nav-item").each(function () {
        var href = $(this).attr('href');
        if (path === href) {
            $(this).closest('li').addClass('aui-nav-selected');
        }
    })
}

function setSidebar() {
    var sidebar = AJS.sidebar('.aui-sidebar');
// Try to expand the sidebar.
    if (sidebar.isCollapsed()) {
        sidebar.expand();
    }

// Event delegation for custom interactions.
    sidebar.$el.on('click', '.clone', function (e) {
        if (sidebar.isCollapsed()) {
            e.preventDefault();
            cloneDialog.show();
        }
    });
}

AJS.toInit(function () {
    setActiveNav();
    setSidebar();
});