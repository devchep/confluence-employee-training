function setDatePicker() {
    const startDatePicker = document.getElementById('start-date-input');
    new AJS.DatePicker(startDatePicker, {
        'overrideBrowserDefault': true,
        hint: 'Выберите дату начала мероприятия'
    });
    const endDatePicker = document.getElementById('end-date-input');
    new AJS.DatePicker(endDatePicker, {
        'overrideBrowserDefault': true,
        hint: 'Выберите дату завершения мероприятия'
    });
}

AJS.toInit(function () {
    setDatePicker();
});

function handlePriceChange(elem) {
    if (elem.value) {
        const filteredValue = elem.value.match(/(\d){1,8}\.?(\d){0,2}/);
        if (filteredValue) {
            $(elem).val(filteredValue[0]);
        } else {
            $(elem).val("");
        }
    }
}

function addCourse() {
    $(".error").empty();
    var course = {
        name: "",
        description: "",
        eventPromoter: "",
        eventLink: "",
        price: 0.0,
        location: "",
        employeesNumber: "",
        startDate: "",
        endDate: ""
    };
    var isFormValid = true;

    var nameInput = $("#name-input");
    course.name = nameInput.val();
    if (course.name.length === 0) {
        isFormValid = false;
        showError(nameInput, "Это обязательное поле");
    }

    var startDateInput = $("#start-date-input");
    if (startDateInput.val().length === 0) {
        isFormValid = false;
        showError(startDateInput, "Это обязательное поле");
    } else if (!/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/.test(startDateInput.val())) {
        isFormValid = false;
        showError(startTimeInput, "Неверный формат даты! Пожалуйста, введите дату в формате yyyy-mm-dd");
    }

    var promoterInput = $("#promoter-input");
    course.eventPromoter = promoterInput.val();
    if (course.eventPromoter.length === 0) {
        isFormValid = false;
        showError(promoterInput, "Это обязательное поле");
    }

    var locationInput = $("#location-input");
    course.location = locationInput.val();
    if (course.location.length === 0) {
        isFormValid = false;
        showError(locationInput, "Это обязательное поле");
    }

    var endDateInput = $("#end-date-input");
    if (endDateInput.val().length !== 0) {
        if (!/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/.test(endDateInput.val())) {
            isFormValid = false;
            showError(endDateInput, "Неверный формат даты! Пожалуйста, введите дату в формате yyyy-mm-dd");
        }
    }

    var startTimeInput = $("#start-time-input");
    if (startTimeInput.val().length === 0) {
        isFormValid = false;
        showError(startTimeInput, "Это обязательное поле");
    } else if (!/^([01]\d|2[0-3]):([0-5]\d)$/.test(startTimeInput.val())) {
        isFormValid = false;
        showError(startTimeInput, "Неверный формат времени! Пожалуйста, введите время в формате hh:mm");
    }

    var endTimeInput = $("#end-time-input");
    if (endTimeInput.val().length !== 0) {
        if (!/^([01]\d|2[0-3]):([0-5]\d)$/.test(endTimeInput.val())) {
            isFormValid = false;
            showError(endTimeInput, "Неверный формат времени! Пожалуйста, введите время в формате hh:mm");
        }
    }

    var employeesNumberInput = $("#employee-number-input");
    if (employeesNumberInput.val().length !== 0) {
        if (!/^\d*$/.test(employeesNumberInput.val())) {
            isFormValid = false;
            showError(employeesNumberInput, "Количество мест должно быть числом");
        }
    }

    course.startDate = startDateInput.val() + " " + startTimeInput.val();
    if (endDateInput.val().length !== 0) course.endDate = endDateInput.val();
    else course.endDate = course.endDate = startDateInput.val();
    if (endTimeInput.val().length !== 0) course.endDate += " " + endTimeInput.val();
    else course.endDate += " " + startTimeInput.val();

    course.price = $("#price-input").val();
    course.description = $("#description-input").val();
    course.eventLink = $("#event-link-input").val();

    if (isFormValid) {
        sendCourseData(course);
    } else {
        AJS.messages.error("#messages", {
            title: "ОШИБКА",
            body: "Исправьте указанные поля и попробуйте еще раз!",
            fadeout: true
        });
    }
}

function showError(inputElem, errorMessage) {
    const errorDiv = inputElem.siblings(".error");
    if (!errorDiv.text()) {
        errorDiv.append(errorMessage);
    }
}

function sendCourseData(course) {
    AJS.$.ajax({
        type: "POST",
        url: "/confluence/rest/training/1.0/courses",
        contentType: "application/json",
        data: JSON.stringify(course),
        success: function () {
            AJS.messages.success("#messages", {
                title: "Успешно",
                body: "Мероприятие успешно добавлено!",
                fadeout: true
            });
            $('form[name=add-course-form]').trigger('reset');
        },
        error: function () {
            AJS.messages.error("#messages", {
                title: "ОШИБКА",
                body: "Мероприятие не было добавлено, попробуйте еще раз!",
                fadeout: true
            });
        }
    });
}
