const MONTHS = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];
const DAYS = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];

const toDelete = new Set();

function DateToReadableDate(startDate) {
    var eventStartDate = new Date(startDate);

    var result = `
        ${eventStartDate.getDate()}
        ${MONTHS[eventStartDate.getMonth()]}
        ${eventStartDate.getFullYear()}
        (${DAYS[eventStartDate.getDay()]}),
        ${eventStartDate.getHours()}:${`0${eventStartDate.getMinutes()}`.slice(-2)}
        `;

    return result
}

function handleDelete() {
    var deleteUrl = "/confluence/rest/training/1.0/courses?";
    toDelete.forEach(item => {
        deleteUrl += `&id=${item}`;
    });
    deleteUrl = deleteUrl.replace('&', '');
    console.log(deleteUrl);

    AJS.$.ajax({
        type: "DELETE",
        url: deleteUrl,
        contentType: "application/json",
        success: function () {
            AJS.messages.success("#messages", {
                title: "Успешно",
                body: "Мероприятия успешно удалены!",
                fadeout: true
            });
            toDelete.forEach(item => {
                $(`#tr-${item}`).remove();
            });
            $("#delete-btn").hide();
        },
        error: function () {
            AJS.messages.error("#messages", {
                title: "ОШИБКА",
                body: "Не удалось удалить выбранные мероприятия, попробуйте еще раз!",
                fadeout: true
            });
        }
    });
}

$(function () {
    function readyDc() {
        $(".event-date").each(function () {
            $(this).text(DateToReadableDate($(this).text()))
        })
        $("#delete-btn").hide();
    }

    $(document).ready(readyDc);

    $(".checkbox").dxCheckBox({
        value: false,
        onValueChanged: function (e) {
            var courseId = $(e.element).attr('id').match(/\d+/g)[0];

            if (e.value) toDelete.add(courseId);
            else toDelete.delete(courseId);

            if (toDelete.size !== 0) {
                $("#delete-btn").show();
                $("#delete-btn").text(`Удалить выбранное (${toDelete.size})`)
            } else {
                $("#delete-btn").hide();
            }
        }
    });
});