
$(function () {
    $("#scheduler").dxScheduler({
        timeZone: "Europe/Moscow",
        dataSource: new DevExpress.data.CustomStore({
            key: "id",
            load: async function () {
                var d = $.Deferred();
                var scheduleData = await $.getJSON("/confluence/rest/training/1.0/courses")
                    .done(function (result) {
                        d.resolve(result);
                    })
                    .fail(function () {
                        throw "Data loading error";
                    });
                return scheduleData.map(function (item) {
                    return {
                        text: item.name,
                        startDate: item.startDate,
                        endDate: item.endDate,
                        courseLink: "/confluence/training/course.action?id=" + item.id
                    }
                });
            }
        }),
        views: ["month"],
        currentView: "month",
        currentDate: new Date(),
        height: 600,
        editing: {
            allowAdding: false,
            allowUpdating: false,
            allowDeleting: false,
            allowDragging: false,
            allowResizing: false
        },
        onAppointmentFormOpening: function (e) {
            e.cancel = true;
            window.location.replace(e.appointmentData.courseLink);
        },
        onAppointmentDblClick: function (e) {
            e.cancel = true;
            window.location.replace(e.appointmentData.courseLink);
        }
    });
});