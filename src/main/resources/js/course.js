const MONTHS = ["января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];
const DAYS = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];

/**
 * @return {string}
 */
function DateToHeaderString(startDate, endDate) {
    var result = "";
    var eventStartDate = new Date(startDate);
    var eventEndDate = new Date(endDate);

    result += `
        ${eventStartDate.getDate()}
        ${MONTHS[eventStartDate.getMonth()]}
        ${eventStartDate.getFullYear()}
        (${DAYS[eventStartDate.getDay()]}),
        ${eventStartDate.getHours()}:${`0${eventStartDate.getMinutes()}`.slice(-2)}
        `;

    if (eventStartDate.getDate() !== eventEndDate.getDate()) {
        result += ` —  
        ${eventEndDate.getDate()}
        ${MONTHS[eventEndDate.getMonth()]}
        ${eventEndDate.getFullYear()}
        (${DAYS[eventEndDate.getDay()]}),
        ${eventEndDate.getHours()}:${`0${eventEndDate.getMinutes()}`.slice(-2)}
        `;
        return result
    }
    if (eventStartDate.getTime() !== eventEndDate.getTime()) {
        result += ` - ${eventEndDate.getHours()}:${`0${eventEndDate.getMinutes()}`.slice(-2)}`
    }

    return result
}

function openForm() {
    $("#apply-application-btn").hide();
    $(".form-popup").show();
    getLeads();
}

function closeForm() {
    $(".form-popup").hide();
    $("#apply-application-btn").show();
}

function addApplication(courseId) {
    $(".error").empty();
    var application = {
        leadName: ""
    };
    var isFormValid = true;

    var leadInput = $("option");
    application.leadName = leadInput.val();
    if (!application.leadName) {
        isFormValid = false;
        const errorDiv = $("#lead-select").siblings(".error");
        if (!errorDiv.text()) {
            errorDiv.append("Пожалуйста, выберите вашего руководителя");
        }
    }

    if (isFormValid) {
        closeForm();
        sendApplicationData(application, courseId);
    }
}

function sendApplicationData(application, courseId) {
    AJS.$.ajax({
        type: "POST",
        url: "/confluence/rest/training/1.0/applications/" + courseId,
        contentType: "application/json",
        data: JSON.stringify(application),
        success: function () {
            AJS.messages.success("#messages", {
                title: "Успешно",
                body: "Заявка успешно отправлена!",
                fadeout: true
            });
        },
        error: function () {
            AJS.messages.error("#messages", {
                title: "ОШИБКА",
                body: "Заявка не была отправлена!",
                fadeout: true
            });
        }
    });
    $('form[name=add-application-form]').trigger('reset');
    $("option").remove();
}

function getLeads() {
    AJS.$.ajax({
        type: "GET",
        url: "/confluence/rest/training/1.0/applications/getLeads",
        contentType: "application/json",
        success: function (data) {
            data.forEach(lead => {
                $("#lead-select").append(
                    `<option value="${lead.name}">${lead.fullName} (${lead.name}) </option>`
                );
            })
            console.log(data)
        },
        error: function () {
            AJS.messages.error("#messages", {
                title: "ОШИБКА",
                body: "Не удалось получить список руководителей!",
                fadeout: true
            });
        }
    });
}