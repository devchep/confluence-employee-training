function deleteApplication(id, elemId) {
    AJS.$.ajax({
        type: "DELETE",
        url: "/confluence/rest/training/1.0/applications/" + id,
        contentType: "application/json",
        success: function () {
            AJS.messages.success("#messages", {
                title: "Успешно",
                body: "Заявка успешно удалена!",
                fadeout: true
            });
            $(`#${elemId}`).remove();
        },
        error: function () {
            AJS.messages.error("#messages", {
                title: "ОШИБКА",
                body: "Ошибка при удалении заявки!",
                fadeout: true
            });
        }
    });
}

function approveApplication(id, elemId) {
    function callback() {
        $(`#${elemId} button`).hide();
        var aplStatus = $(`#${elemId} #apl-status`);
        aplStatus.text("Подтверждена");
    }

    changeApplicationStatus(id, 1, elemId, callback);
}

function rejectApplication(id, elemId) {
    function callback() {
        $(`#${elemId} button`).each(function () {
            $(this).hide()
        });
        var aplStatus = $(`#${elemId} #apl-status`);
        aplStatus.text("Отменена");
    }

    changeApplicationStatus(id, 2, elemId, callback);
}

function changeApplicationStatus(id, status, elemId, callback) {
    AJS.$.ajax({
        type: "PUT",
        url: `/confluence/rest/training/1.0/applications/${id}?status=${status}`,
        contentType: "application/json",
        success: function () {
            AJS.messages.success("#messages", {
                title: "Успешно",
                body: "Статус заявки успешно изменен!",
                fadeout: true
            });
            callback();
        },
        error: function () {
            AJS.messages.error("#messages", {
                title: "ОШИБКА",
                body: "Ошибка при изменении статуса заявки!",
                fadeout: true
            });
        }
    });
}