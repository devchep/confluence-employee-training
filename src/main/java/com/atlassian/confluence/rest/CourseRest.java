package com.atlassian.confluence.rest;

import com.atlassian.confluence.ao.Course;
import com.atlassian.confluence.model.CourseModel;
import com.atlassian.confluence.service.AccessService;
import com.atlassian.confluence.service.CourseService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/courses")
public class CourseRest {
    private final CourseService courseService;

    @Inject
    public CourseRest(CourseService courseService) {
        this.courseService = courseService;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addCourse(CourseModel courseModel) {
        courseService.addCourse(courseModel);
        return Response.ok().build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCourses() {
        return Response.ok(courseService.getAllCourses()).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCourse(@PathParam("id") int id) {
        return Response.ok(courseService.getCourse(id)).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCourse(CourseModel courseModel) {
        courseService.updateCourse(courseModel);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteCourse(@PathParam("id") int id) {
        courseService.deleteCourse(id);
        return Response.ok().build();
    }

    @DELETE
    public Response deleteCourses(@QueryParam("id") final List<Integer> courseIds) {
        if (courseIds != null) {
            courseService.deleteCourses(courseIds);
            return Response.ok().build();
        }
        throw new WebApplicationException(Response.Status.NOT_FOUND);
    }
}