package com.atlassian.confluence.rest;

import com.atlassian.confluence.ao.Application;
import com.atlassian.confluence.model.ApplicationModel;
import com.atlassian.confluence.service.AccessService;
import com.atlassian.confluence.service.ApplicationService;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.user.EntityException;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;

@Path("/applications")
public class ApplicationRest {
    private final ApplicationService applicationService;
    private final UserManager userManager;
    private final AccessService accessService;

    @Inject
    public ApplicationRest(UserManager userManager, AccessService accessService, ApplicationService applicationService) {
        this.applicationService = applicationService;
        this.userManager = userManager;
        this.accessService = accessService;
    }

    @POST
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addApplication(@PathParam("id") int courseId, ApplicationModel applicationModel) {
        if (userManager.getRemoteUserKey() == null) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED);
        }
        applicationModel.setUserName(Objects.requireNonNull(userManager.getRemoteUser()).getUsername());
        applicationModel.setUserKey(Objects.requireNonNull(userManager.getRemoteUserKey()).getStringValue());

        applicationModel.setLeadName(applicationModel.getLeadName());
        UserProfile userLead = userManager.getUserProfile(applicationModel.getLeadName());
        applicationModel.setLeadKey(Objects.requireNonNull(userLead).getUserKey().getStringValue());

        Application application = applicationService.addApplication(courseId, applicationModel);
        return Response.ok().build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getApplications() {
        if (accessService.isHR()) {
            return Response.ok(applicationService.getAllApplications()).build();
        }
        throw new WebApplicationException(Response.Status.NOT_FOUND);
    }

    @PUT
    @Path("{id}")
    public Response changeApplicationStatus(@PathParam("id") int id, @QueryParam("status") int status) {
        if (!accessService.isLead()) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        applicationService.changeApplicationStatus(id, status);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    public Response deleteApplication(@PathParam("id") int id) {
        applicationService.deleteApplication(id);
        return Response.ok().build();
    }

    @GET
    @Path("/getLeads")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLeads() throws EntityException {
        return Response.ok(applicationService.getAllLeads()).build();
    }
}
