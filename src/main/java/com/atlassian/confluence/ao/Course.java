package com.atlassian.confluence.ao;

import net.java.ao.Entity;
import net.java.ao.OneToMany;
import net.java.ao.schema.StringLength;

import java.util.Date;

public interface Course extends Entity {
    String getName();
    void setName(String name);

    @StringLength(StringLength.UNLIMITED)
    String getDescription();
    void setDescription(String description);

    String getEventPromoter();
    void setEventPromoter(String eventPromoter);

    String getEventLink();
    void setEventLink(String eventLink);

    double getPrice();
    void setPrice(double price);

    int getEmployeesNumber();
    void setEmployeesNumber(int employeesNumber);

    String getLocation();
    void setLocation(String location);

    Date getStartDate();
    void setStartDate(Date startDate);

    Date getEndDate();
    void setEndDate(Date endDate);

    @OneToMany
    Application[] getApplications();
}
