package com.atlassian.confluence.ao;

import net.java.ao.Entity;
import net.java.ao.schema.Default;
import net.java.ao.schema.StringLength;

import java.util.Date;

public interface Application extends Entity {

    @StringLength(StringLength.UNLIMITED)
    String getUserKey();
    void setUserKey(String userKey);

    @StringLength(StringLength.UNLIMITED)
    String getLeadKey();
    void setLeadKey(String leadKey);

    String getLeadName();
    void setLeadName(String leadName);

    String getUserName();
    void setUserName(String userName);

    @Default("0")
    int getStatus();
    void setStatus(int status);

    Date getCreatedAt();
    void setCreatedAt(Date createdAt);

    Course getCourse();
    void setCourse(Course course);
}
