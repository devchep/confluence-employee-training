package com.atlassian.confluence.model;

import com.atlassian.confluence.ao.Course;
import com.atlassian.confluence.model.adapters.DateAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

@XmlRootElement
public class CourseModel {
    @XmlElement
    private int id;
    @XmlElement
    private String name;
    @XmlElement
    private String description;
    @XmlElement
    private String eventPromoter;
    @XmlElement
    private String eventLink;
    @XmlElement
    private double price;
    @XmlElement
    private int employeesNumber;
    @XmlElement
    private String location;
    @XmlElement
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date startDate;
    @XmlElement
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date endDate;

    private CourseModel() {
    }

    public CourseModel(Course course) {
        this.id = course.getID();
        this.name = course.getName();
        this.description = course.getDescription();
        this.eventPromoter = course.getEventPromoter();
        this.price = course.getPrice();
        this.eventLink = course.getEventLink();
        this.employeesNumber = course.getEmployeesNumber();
        this.location = course.getLocation();
        this.startDate = course.getStartDate();
        this.endDate = course.getEndDate();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEventLink() {
        return eventLink;
    }

    public void setEventLink(String eventLink) {
        this.eventLink = eventLink;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        if (endDate != null) {
            return endDate;
        } else {
            return startDate;
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setEventPromoter(String eventPromoter) {
        this.eventPromoter = eventPromoter;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setEmployeesNumber(int employeesNumber) {
        this.employeesNumber = employeesNumber;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setStartDate(Date startDate) {

        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getEventPromoter() {
        return eventPromoter;
    }

    public double getPrice() {
        return price;
    }

    public int getEmployeesNumber() {
        return employeesNumber;
    }

    public String getLocation() {
        return location;
    }
}