package com.atlassian.confluence.model.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAdapter extends XmlAdapter<String, Date> {

    private static final String CUSTOM_FORMAT_STRING = "yyyy-MM-dd HH:mm";

    @Override
    public String marshal(Date v) {
        if(null == v) {
            return null;
        }
        return new SimpleDateFormat(CUSTOM_FORMAT_STRING).format(v);
    }

    @Override
    public Date unmarshal(String v) throws ParseException {
        if(v.replaceAll("\\s","").length() == 0) {
            return null;
        }
        return new SimpleDateFormat(CUSTOM_FORMAT_STRING).parse(v);
    }

}