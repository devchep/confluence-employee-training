package com.atlassian.confluence.model;

import com.atlassian.confluence.ao.Application;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@XmlRootElement
public class ApplicationModel {
    @XmlElement
    private int id;
    @XmlElement
    private String userKey;
    @XmlElement
    private String leadKey;
    @XmlElement
    private String userName;
    @XmlElement
    private String leadName;
    @XmlElement
    private int status;
    @XmlElement
    private Date createdAt;
    @XmlElement
    private CourseModel courseModel;

    public ApplicationModel() {
    }

    public ApplicationModel(Application application) {
        this.id = application.getID();
        this.userKey = application.getUserKey();
        this.leadKey = application.getLeadKey();
        this.userName = application.getUserName();
        this.leadName = application.getLeadName();
        this.status = application.getStatus();
        this.createdAt = application.getCreatedAt();
        this.courseModel = new CourseModel(application.getCourse());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getLeadKey() {
        return leadKey;
    }

    public void setLeadKey(String leadKey) {
        this.leadKey = leadKey;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLeadName() {
        return leadName;
    }

    public void setLeadName(String leadName) {
        this.leadName = leadName;
    }

    public int getStatus() {
        return status;
    }

    public void setIsApproved(int status) {
        this.status = status;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public CourseModel getCourseModel() {
        return courseModel;
    }

    public void setCourseModel(CourseModel courseModel) {
        this.courseModel = courseModel;
    }
}
