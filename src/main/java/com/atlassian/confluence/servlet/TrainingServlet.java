package com.atlassian.confluence.servlet;

import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TrainingServlet extends HttpServlet {
    private final PageBuilderService pageBuilderService;
    private final UserManager userManager;

    @Inject
    public TrainingServlet(@ComponentImport PageBuilderService pageBuilderService, @ComponentImport UserManager userManager) {
        this.pageBuilderService = pageBuilderService;
        this.userManager = userManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String webResource;
        if (this.userManager.getRemoteUsername() != null) {
            webResource = "com.atlassian.confluence.trainingPlugin:trainingPlugin-resources";
        } else {
            webResource = "com.atlassian.confluence.trainingPlugin:no-user";
        }
        pageBuilderService
                .assembler()
                .resources()
                .requireWebResource(webResource);
        Map<String, Object> contextMap = new HashMap();
        contextMap.put("myVar", Math.random());
        String context = VelocityUtils.getRenderedTemplate("templates/CoursesSchedule.vm", contextMap);
        resp.setContentType("text/html");
        resp.getWriter().write(context);
    }

}