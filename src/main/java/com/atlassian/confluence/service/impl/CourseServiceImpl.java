package com.atlassian.confluence.service.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.ao.Application;
import com.atlassian.confluence.ao.Course;
import com.atlassian.confluence.model.CourseModel;
import com.atlassian.confluence.service.CourseService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionCallback;
import net.java.ao.Query;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
public class CourseServiceImpl implements CourseService {
    @ComponentImport
    private final ActiveObjects ao;

    @Inject
    public CourseServiceImpl(ActiveObjects ao) {
        this.ao = ao;
    }

    @Override
    public Course addCourse(CourseModel courseModel) {
        return ao.executeInTransaction(() -> {
            Course course = ao.create(Course.class);
            return setCourse(courseModel, course);
        });
    }

    @Override
    public Course updateCourse(CourseModel courseModel) {
        return ao.executeInTransaction(() -> {
            Course course = ao.get(Course.class, courseModel.getId());
            return setCourse(courseModel, course);
        });
    }

    @Override
    public CourseModel getCourse(int id) {
        return ao.executeInTransaction(new TransactionCallback<CourseModel>() {
            @Override
            public CourseModel doInTransaction() {
                return new CourseModel(ao.get(Course.class, id));
            }
        });
    }

    @Override
    public void deleteCourse(int id) {
        ao.executeInTransaction(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction() {
                Course course = ao.get(Course.class, id);
                ao.delete(course);
                return null;
            }
        });
    }

    @Override
    public ArrayList<CourseModel> getAllCourses() {
        return ao.executeInTransaction(new TransactionCallback<ArrayList<CourseModel>>() {
            @Override
            public ArrayList<CourseModel> doInTransaction() {
                Course[] courses = ao.find(Course.class);
                ArrayList<CourseModel> courseModels = new ArrayList<>();
                for (Course course : courses) {
                    courseModels.add(new CourseModel(course));
                }
                return courseModels;
            }
        });
    }

    @Override
    public void deleteCourses(List<Integer> courseIds) {
        ao.executeInTransaction(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction() {
                for (int id :
                        courseIds) {
                    Application[] applications = ao.find(Application.class, Query.select().where("COURSE_ID LIKE ?", id));
                    for (Application application : applications) {
                        ao.delete(application);
                    }
                    Course course = ao.get(Course.class, id);
                    if (course != null) {
                        ao.delete(course);
                    }
                }
                return null;
            }
        });
    }

    private Course setCourse(CourseModel courseModel, Course course) {
        course.setName(courseModel.getName());
        course.setDescription(courseModel.getDescription());
        course.setEventPromoter(courseModel.getEventPromoter());
        course.setEventLink(courseModel.getEventLink());
        course.setEmployeesNumber(courseModel.getEmployeesNumber());
        course.setPrice(courseModel.getPrice());
        course.setLocation(courseModel.getLocation());
        course.setStartDate(courseModel.getStartDate());
        course.setEndDate(courseModel.getEndDate());

        course.save();
        return course;
    }
}