package com.atlassian.confluence.service.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.confluence.ao.Application;
import com.atlassian.confluence.ao.Course;
import com.atlassian.confluence.model.ApplicationModel;
import com.atlassian.confluence.model.UserModel;
import com.atlassian.confluence.service.ApplicationService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.user.EntityException;
import com.atlassian.user.Group;
import com.atlassian.user.GroupManager;
import com.atlassian.user.search.page.Pager;
import net.java.ao.Query;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Date;

@Named
public class ApplicationServiceImpl implements ApplicationService {
    private final String LEAD_GROUP = "leads-group";

    @ComponentImport
    private final ActiveObjects ao;

    @ComponentImport
    private final UserManager userManager;

    @ComponentImport
    private final GroupManager groupManager;

    @Inject
    public ApplicationServiceImpl(ActiveObjects ao, UserManager userManager, GroupManager groupManager) {
        this.ao = ao;
        this.userManager = userManager;
        this.groupManager = groupManager;
    }

    @Override
    public ArrayList<UserModel> getAllLeads() throws EntityException {
        Group group = groupManager.getGroup(LEAD_GROUP);
        Pager<String> leadNamesPager = groupManager.getMemberNames(group);
        ArrayList<String> leadNames = new ArrayList<>(leadNamesPager.getCurrentPage());

        ArrayList<UserModel> leadUserModels = new ArrayList<>();
        for (String leadName : leadNames) {
            UserProfile leadProfile = userManager.getUserProfile(leadName);
            assert leadProfile != null;
            leadUserModels.add(new UserModel(leadProfile.getUsername(), leadProfile.getFullName()));
        }
        return leadUserModels;
    }

    @Override
    public Application addApplication(int courseId, ApplicationModel applicationModel) {
        return ao.executeInTransaction(new TransactionCallback<Application>() {
            @Override
            public Application doInTransaction() {
                Application[] applications = ao.find(Application.class, Query.select().where(
                        "COURSE_ID LIKE ? AND USER_KEY LIKE ?",
                        courseId, applicationModel.getUserKey()));

                if (applications.length != 0) {
                    throw new WebApplicationException(Response.Status.CONFLICT);
                }

                Application application = ao.create(Application.class);

                application.setUserName(applicationModel.getUserName());
                application.setUserKey(applicationModel.getUserKey());
                application.setLeadName(applicationModel.getLeadName());
                application.setLeadKey(applicationModel.getLeadKey());
                application.setCreatedAt(new Date());
                application.setCourse(ao.get(Course.class, courseId));

                application.save();
                return application;
            }
        });
    }

    @Override
    public void changeApplicationStatus(int id, int status) {
        ao.executeInTransaction(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction() {
                Application application = ao.get(Application.class, id);
                application.setStatus(status);

                application.save();
                return null;
            }
        });
    }

    @Override
    public ArrayList<ApplicationModel> getAllApplications() {
        return ao.executeInTransaction(new TransactionCallback<ArrayList<ApplicationModel>>() {
            @Override
            public ArrayList<ApplicationModel> doInTransaction() {
                Application[] applications = ao.find(Application.class);
                ArrayList<ApplicationModel> applicationModels = new ArrayList<>();
                for (Application application : applications) {
                    applicationModels.add(new ApplicationModel(application));
                }
                return applicationModels;
            }
        });
    }

    @Override
    public void deleteApplication(int id) {
        ao.executeInTransaction(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction() {
                Application application = ao.get(Application.class, id);

                if (application == null) {
                    throw new WebApplicationException(Response.Status.NOT_FOUND);
                }

                ao.delete(application);
                return null;
            }
        });
    }

    @Override
    public ArrayList<ApplicationModel> getUserApplications(String userKey) {
        return ao.executeInTransaction(new TransactionCallback<ArrayList<ApplicationModel>>() {
            @Override
            public ArrayList<ApplicationModel> doInTransaction() {
                Application[] applications = ao.find(Application.class, Query.select().where("USER_KEY LIKE ?", userKey));
                ArrayList<ApplicationModel> applicationModels = new ArrayList<>();
                for (Application application : applications) {
                    applicationModels.add(new ApplicationModel(application));
                }
                return applicationModels;
            }
        });
    }

    @Override
    public ArrayList<ApplicationModel> getLeadApplications(String leadKey) {
        return ao.executeInTransaction(new TransactionCallback<ArrayList<ApplicationModel>>() {
            @Override
            public ArrayList<ApplicationModel> doInTransaction() {
                Application[] applications = ao.find(Application.class, Query.select().where("LEAD_KEY LIKE ?", leadKey));
                ArrayList<ApplicationModel> applicationModels = new ArrayList<>();
                for (Application application : applications) {
                    applicationModels.add(new ApplicationModel(application));
                }
                return applicationModels;
            }
        });
    }
}
