package com.atlassian.confluence.service.impl;

import com.atlassian.confluence.service.AccessService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class AccessServiceImpl implements AccessService {
    private final String USER_GROUP = "confluence-users";
    private final String LEAD_GROUP = "leads-group";
    private final String HR_GROUP = "hr-group";

    @ComponentImport
    private final UserManager userManager;

    @Inject
    public AccessServiceImpl(UserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    public boolean hasAccess() {
        return userManager.getRemoteUser() != null;
    }

    @Override
    public boolean isLead() {
        return userManager.isUserInGroup(userManager.getRemoteUserKey(), LEAD_GROUP);
    }

    @Override
    public boolean isAdmin() {
        return userManager.isAdmin(userManager.getRemoteUserKey());
    }

    @Override
    public boolean isHR() {
        return userManager.isUserInGroup(userManager.getRemoteUserKey(), HR_GROUP);
    }

    @Override
    public boolean isUser() {
        return userManager.isUserInGroup(userManager.getRemoteUserKey(), USER_GROUP);
    }

    @Override
    public UserProfile getCurrentUser(){
        return userManager.getRemoteUser();
    }
}
