package com.atlassian.confluence.service;

import com.atlassian.confluence.ao.Course;
import com.atlassian.confluence.model.CourseModel;

import java.util.ArrayList;
import java.util.List;

public interface CourseService {
    Course addCourse(CourseModel courseModel);
    Course updateCourse(CourseModel courseModel);
    void deleteCourse(int id);
    CourseModel getCourse(int id);
    ArrayList<CourseModel> getAllCourses();
    void deleteCourses(List<Integer> courseIds);
}
