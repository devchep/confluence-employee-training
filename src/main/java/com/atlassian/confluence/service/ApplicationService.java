package com.atlassian.confluence.service;

import com.atlassian.confluence.ao.Application;
import com.atlassian.confluence.model.ApplicationModel;
import com.atlassian.confluence.model.UserModel;
import com.atlassian.user.EntityException;

import java.util.ArrayList;

public interface ApplicationService {
    Application addApplication(int courseId, ApplicationModel applicationModel);
    void changeApplicationStatus(int id, int status);
    ArrayList<ApplicationModel> getAllApplications();
    ArrayList<UserModel> getAllLeads() throws EntityException;
    void deleteApplication(int id);
    ArrayList<ApplicationModel> getUserApplications(String userKey);
    ArrayList<ApplicationModel> getLeadApplications(String leadKey);
}
