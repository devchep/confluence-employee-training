package com.atlassian.confluence.service;

import com.atlassian.sal.api.user.UserProfile;

public interface AccessService {
    boolean hasAccess();
    boolean isUser();
    boolean isLead();
    boolean isHR();
    boolean isAdmin();
    UserProfile getCurrentUser();
}