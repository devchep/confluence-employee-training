package com.atlassian.confluence.action;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.model.CourseModel;
import com.atlassian.confluence.service.AccessService;
import com.atlassian.confluence.service.CourseService;

import java.util.Date;

public class EditCourseAction extends ConfluenceActionSupport {
    public int id;

    private final AccessService accessService;
    private final CourseService courseService;

    public EditCourseAction(AccessService accessService, CourseService courseService) {
        this.accessService = accessService;
        this.courseService = courseService;
    }

    public CourseModel getCourse() {
        return courseService.getCourse(id);
    }

    public String toDateString(Date date) {
        return date.toString().substring(0, 10);
    }

    public String toTimeString(Date date) {
        return date.toString().substring(11, 16);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean hasAccess() {
        return accessService.hasAccess();
    }

    public boolean isUser() {
        return accessService.isUser();
    }

    public boolean isAdmin() {
        return accessService.isAdmin();
    }

    public boolean isHR() {
        return accessService.isHR();
    }
}
