package com.atlassian.confluence.action;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.model.ApplicationModel;
import com.atlassian.confluence.service.AccessService;
import com.atlassian.confluence.service.ApplicationService;

import java.util.ArrayList;

public class ApplicationsAction extends ConfluenceActionSupport {
    private final AccessService accessService;
    private final ApplicationService applicationService;

    public ApplicationsAction(AccessService accessService, ApplicationService applicationService) {
        this.accessService = accessService;
        this.applicationService = applicationService;
    }

    public boolean hasAccess() {
        return accessService.hasAccess();
    }
    public boolean isUser() {
        return accessService.isUser();
    }
    public boolean isAdmin() { return accessService.isAdmin(); }
    public boolean isLead() {
        return accessService.isLead();
    }

    public ArrayList<ApplicationModel> getUserApplications() {
        return applicationService.getUserApplications(
                accessService.getCurrentUser()
                        .getUserKey()
                        .getStringValue()
        );
    }

    public ArrayList<ApplicationModel> getLeadApplications() {
        return applicationService.getLeadApplications(
                accessService.getCurrentUser()
                        .getUserKey()
                        .getStringValue()
        );
    }
}
