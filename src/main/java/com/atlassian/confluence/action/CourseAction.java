package com.atlassian.confluence.action;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.model.CourseModel;
import com.atlassian.confluence.model.UserModel;
import com.atlassian.confluence.service.AccessService;
import com.atlassian.confluence.service.ApplicationService;
import com.atlassian.confluence.service.CourseService;
import com.atlassian.user.EntityException;

import java.util.ArrayList;

public class CourseAction extends ConfluenceActionSupport {
    public int id;

    private final AccessService accessService;
    private final CourseService courseService;

    public CourseAction(AccessService accessService, CourseService courseService) {
        this.accessService = accessService;
        this.courseService = courseService;
    }

    public CourseModel getCourse() {
        return courseService.getCourse(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean hasAccess() {
        return accessService.hasAccess();
    }

    public boolean isUser() {
        return accessService.isUser();
    }

    public boolean isAdmin() {
        return accessService.isAdmin();
    }

    public boolean isLead() {
        return accessService.isLead();
    }

    public boolean isHR() {
        return accessService.isHR();
    }
}
