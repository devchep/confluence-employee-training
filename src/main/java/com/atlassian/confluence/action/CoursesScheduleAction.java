package com.atlassian.confluence.action;

import com.atlassian.confluence.ao.Course;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.model.CourseModel;
import com.atlassian.confluence.service.AccessService;
import com.atlassian.confluence.service.CourseService;

import java.util.ArrayList;

public class CoursesScheduleAction extends ConfluenceActionSupport {
    public final AccessService accessService;
    private final CourseService courseService;

    public CoursesScheduleAction(AccessService accessService, CourseService courseService){
        this.accessService = accessService;
        this.courseService = courseService;
    }

    public ArrayList<CourseModel> getAllCourses() {
        return courseService.getAllCourses();
    }

    public boolean hasAccess() {
        return accessService.hasAccess();
    }

    public boolean isUser() {
        return accessService.isUser();
    }

    public boolean isLead() {
        return accessService.isLead();
    }

    public boolean isAdmin() {
        return accessService.isAdmin();
    }

    public boolean isHR() {
        return accessService.isHR();
    }
}
