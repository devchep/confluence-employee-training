package com.atlassian.confluence.action;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.service.AccessService;

import javax.inject.Inject;

public class AddCourseAction extends ConfluenceActionSupport {
    public final AccessService accessService;

    @Inject
    public AddCourseAction(AccessService accessService) {
        this.accessService = accessService;
    }

    public boolean hasAccess() {
        return accessService.hasAccess();
    }

    public boolean isUser() {
        return accessService.isUser();
    }

    public boolean isAdmin() {
        return accessService.isAdmin();
    }

    public boolean isHR() {
        return accessService.isHR();
    }
}
